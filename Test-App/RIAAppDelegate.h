//
//  RIAAppDelegate.h
//  Test-App
//
//  Created by Łukasz Juskowiak on 22/12/13.
//  Copyright (c) 2013 RIA Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface RIAAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
