//
//  main.m
//  Test-App
//
//  Created by Łukasz Juskowiak on 22/12/13.
//  Copyright (c) 2013 RIA Studio. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
